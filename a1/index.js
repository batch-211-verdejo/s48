console.log("Hello World");


// Mock Database
let posts = [];

// Post ID
let count = 1;

// Add post

document.querySelector("#form-add-post").addEventListener("submit", (event) => {

    event.preventDefault() //prevent the page reloading

    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })
    count++
    console.log(posts)
    alert("Successfully added!")
    // invoke a function to show post later
    showPosts();
});

// Edit post
const showPosts = () => {
    let postEntries = "";

    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
            <h3 id="post-title-${post.id}"> ${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onClick="editPost('${post.id}')">Edit</button>
            <button onClick="deletePost('${post.id}')">delete</button>
            </div>
        
        `
    })
    console.log(postEntries)
    document.querySelector("#div-post-entries").innerHTML = postEntries
};

// Edit Post Button

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
    
};

/*
	s48 Activity
	1. Add a new function in index.js called deletePost().
	2. This function should be able to receive the id number of the post:
		a. Remove the item with the same id number from the posts array
		b. Note: you can use array methods such as filter() or splice()
		c. Then, remove the element from the DOM by first selecting the element and using the remove() method	
*/

// Delete Post Button
const deletePost = (id) => {

    document.querySelector(`#post-${id}`).remove();
    console.log(posts);
    
};



// Update post

document.querySelector("#form-edit-post").addEventListener("submit", (event) => {

    event.preventDefault()

    for (let i = 0; i < posts.length; i++) {

        if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPosts(posts)
            alert("Successfullly Updated!")
            break;
        }
    }
});

